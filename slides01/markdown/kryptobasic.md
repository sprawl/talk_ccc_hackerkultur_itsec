# Kryptobasics

* Hashes
* Verschlüsselung
* Signatur


# Hashes
Daten => Hashfunktion => Hash


# Hashes
große Daten => Hashfunktion => kleine Daten


# Hashes
geheime Daten => Hashfunktion => veröffentlichbare Daten



# Hashes werden genutzt zur Prüfung von Daten
* Integritätsprüfung
* Schutz von Passwörtern
* effizentes Suchen


eine (kurze) Prüfsumme berechnen

*Beispiel:*

Prüfsumme einer ISBN, Zyklische Redundanzprüfung zur Erkennung von Übertragungsfehlern von Dateien


einen Inhalt nahezu eindeutig (aber immer noch „kurz“) zu identifizieren, ohne etwas über den Inhalt zu verraten

*Beispiel:*

Passwortdatenbank


einem komplexen Objekt einen Speicherort zuzuweisen

*Beispiel:*

„Max Mustermann“ wird im Ordner „m“ abgelegt, dem ersten Buchstaben des Nachnamens.


# Was sollte eine Hashfunktion haben?
* Kollisionsresistenz
* Einwegeigenschaft


# übliche Hashfunkton
* MD5 (gebrochen)
* SHA-1 (angebrochen)
* SHA-2
* SHA-3



# Symmetrisches Kryptosystem
<img class="fsimg" src="images/Orange_blue_symmetric_cryptography_de.svg" />


# übliche symmetrische Kryptosysteme
* (Triple-)DES (gebrochen)
* AES
* Blowfish
* Camellia


# Verschlüsselung
<img class="fsimg" src="images/Orange_blue_public_key_cryptography_de.svg" />


# Asymmetrisches Kryptosystem
<img class="fsimg" src="images/Orange_blue_public_private_keygeneration_de.svg" />


# Signatur
<img class="fsimg" src="images/Orange_blue_digital_signature_de.svg" />


# übliche asymmetrische Kryptosysteme
* RSA
* Elgamal


# Problem bei Kryptographie

* Schwächen in der Mathematik
* Schwächen in der Implementierung
* Zugriff auf Key/Geheimnis
