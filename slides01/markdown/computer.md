
# Grundlagen Computer



# Wie funktioniert ein PC?

… und welche Probleme gibt es bzgl. der Arbeit von Staatsanwälten?<!-- .element: class="fragment" -->


# Rechern sind "Kopiermaschinen"


Computer

<img class="fsimg" src="images/copyingmachine1.svg" />


Festplatte

<img class="fsimg" src="images/copyingmachine2.svg" />


Computer

<img class="fsimg" src="images/copyingmachine1.svg" />


CPU

<img class="fsimg" src="images/copyingmachine3.svg" />


Computer

<img class="fsimg" src="images/copyingmachine1.svg" />


# alles sind Nullen und Einsen
# alles ist fälschbar<!-- .element: class="fragment" -->


# alles sind Nullen und Einsen

## Bilder<!-- .element: class="fragment" -->
## Videos<!-- .element: class="fragment" -->
## PDFs<!-- .element: class="fragment" -->


# alles sind Nullen und Einsen

## Metadaten von Dokumenten<!-- .element: class="fragment" -->
## Logfiles<!-- .element: class="fragment" -->
## Zeitstempel einer Datei<!-- .element: class="fragment" -->



# Daten & Metadaten

<img src="images/metadata.jpg" />


# Daten & Metadaten
    ExifIFD:Time:DateTimeOriginal=2013:10:19 14:55:54
    IFD0:Camera:Make=SAMSUNG
    IFD0:Camera:Model=NX10
    IFD0:Camera:FirmwareName=NX10_FW_X_F1103221
    IFD0:Camera:LensType=Samsung NX 30mm F2 Pancake
    IPTC:Author:By-line=Martin Vietz


# alles sind Nullen und Einsen
# alles ist fälschbar
### *fast alles<!-- .element: class="fragment" -->


# was nicht?

## war Zugriff von Interessierten möglich?<!-- .element: class="fragment" -->

Beweisbarkeit/Ausschluß?<!-- .element: class="fragment" -->

## Signaturen / Verschlüsselung<!-- .element: class="fragment" -->
*solange nicht gebrochen<!-- .element: class="fragment" -->


