# Hacken



## Klassiker:
### Wie werde ich Hacker?
* mindset<!-- .element: class="fragment" -->
* lernen<!-- .element: class="fragment" -->
* lernen<!-- .element: class="fragment" -->
* lernen<!-- .element: class="fragment" -->



# Gesetze hacken?
* kreative Buchhaltung <!-- .element: class="fragment" -->
* Prozessverzögerungen <!-- .element: class="fragment" -->
* Gegendemos <!-- .element: class="fragment" -->
* \#dietotenkommen <!-- .element: class="fragment" -->



# Warum geht das überhaupt?


# Komplexität


| Gesetz          | Zeilen |
|-----------------|--------|
| Grundgesetz     |  1 303 |
| Strafgesetzbuch |  6 162 |
| BGB             | 13 117 |


| Softwareprojekt  |         Zeilen |
|------------------|----------------|
| Linux Kernel 4.0 |     17 847 304 |
| Mozilla Firefox  | ca. 28 500 000 |
| Windows XP       | ca. 40 000 000 |
| Mac OS X 10.4    | ca. 86 000 000 |



# Beispiele
