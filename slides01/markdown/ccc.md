# CCC

* intergalaktische Gemeinschaft
* kreativer Umgang mit Technik
* seit 12. September 1981

<img width="50%" src="images/Logo_CCC.svg" />


# Entropia

* Lokale Niederlassung des CCC in Karlsruhe
* Räume im Gewerbehof am Liedelplatz
* seit April 1998

<img width="30%" src="images/Teebeutel1.svg" />


# ... e.V.

## CCC
* Gegründet: 14. April 1986
* Sitz: Hamburg
* Mitglieder: über 6000

## Entropia
* Gegründet: 11. April 1999
* Sitz: Karlsruhe
* Mitglieder: ca 100


# ... e.V.

* nötig aus rechtlichen Gründen
* im Clubleben nicht relevant


# was ist dann relevant?


# Hackerethik

* Steven Levy -  Hackers - 1984
	* geht zurück auf die Subkultur am MIT in den 1960er
* Erweiterungen durch den CCC in den 1980er


## Der Zugang zu Computern und allem, was einem zeigen kann, wie diese Welt funktioniert, sollte unbegrenzt und vollständig sein.


## Alle Informationen müssen frei sein.


## Mißtraue Autoritäten – fördere Dezentralisierung.


## Beurteile einen Hacker nach dem, was er tut, und nicht nach üblichen Kriterien wie Aussehen, Alter, Herkunft, Spezies, Geschlecht oder gesellschaftliche Stellung.


## Man kann mit einem Computer Kunst und Schönheit schaffen.


## Computer können dein Leben zum Besseren verändern.


## Mülle nicht in den Daten anderer Leute.


## Öffentliche Daten nützen, private Daten schützen.


# Grenzbereiche

* White Hat vs. Black Hat
	* die Welt ist grau
* Hackerparagraf StGB §202c
* Öffentliche Daten vs. private Daten


# Geschichte des CCC

* Btx/Haspa-Hack
* GSM-Hack
* Blinkenlights
* Nedap-Wahlcomputer
* Staatstrojaner


# was tut der CCC heute?


# Veranstaltungen
* Chaos Communication Congress
* Chaos Communication Camp
* GPN
* … (viele weitere)


# Vorträge
* Vortragsreihen
* auf Anfrage
* Chaos macht Schule

# Publikationen
* Die Datenschleuder
* Radio / Podcasts


# Gutachten / Beratung / Stellungnahmen
* Politiker
* Bundesverfassungsgericht
* Presse
